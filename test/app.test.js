const request = require("supertest");
const App = require("../src/app");

describe("app", () => {
  let app;
  const encodedCommand =
    "token=gIkuvaNzQIHg97ATvDxqgjtO" +
    "&team_id=T0001" +
    "&team_domain=example" +
    "&enterprise_id=E0001" +
    "&enterprise_name=Globular%20Construct%20Inc" +
    "&channel_id=C2147483705" +
    "&channel_name=test" +
    "&user_id=U2147483697" +
    "&user_name=Steve" +
    "&command=/addskill" +
    "&text=my%20skill" +
    "&response_url=https://hooks.slack.com/commands/1234/5678" +
    "&trigger_id=13345224609.738474920.8088930838d88f008e0";

  const responseMsg = { text: "Skill updated" };

  beforeEach(() => {
    app = App();
  });
  it("returns status code 200 on empty request", () =>
    request(app)
      .post("/skillfinder-slack")
      .set("Accept", "application/x-www-form-urlencoded")
      .expect(200));

  it("returns a successful message when addskill command runs", () =>
    request(app)
      .post("/skillfinder-slack")
      .send(encodedCommand)
      .set("Accept", "application/x-www-form-urlencoded")
      .expect("Content-Type", /json/)
      .expect(200)
      .then(res => expect(res.body).toEqual(responseMsg)));
});

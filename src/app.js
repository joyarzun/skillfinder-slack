const express = require("express");
const bodyParser = require("body-parser");

module.exports = () => {
  const app = express();
  app.use(bodyParser.urlencoded({ extended: true }));

  app.post("/skillfinder-slack", function(req, res) {
    if (req.body.command == "/addskill") {
      return res.status(200).json({ text: "Skill updated" });
    }
    return res.status(200).end();
  });

  return app;
};
